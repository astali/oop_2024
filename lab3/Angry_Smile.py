import pygame
from pygame.draw import *

pygame.init()

# Цвета (R, G, B)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
# Main coords
CENTRE = (200, 175)


def Eyes():
    circle(screen, RED, (150, 150), 20)
    circle(screen, RED, (250, 150), 15)
    circle(screen, BLACK, (150, 150), 10)
    circle(screen, BLACK, (250, 150), 5)


def Face():
    circle(screen, BLACK, (200, 175), 100, 5)
    circle(screen, YELLOW, (200, 175), 95)


def Eyebrows():
    line(screen, BLACK, [110, 110], [170, 130], 10)
    line(screen, BLACK, [220, 140], [300, 90], 10)
    #polygon(screen, BLACK, [(100,100),(200,50),(300,100),(100,100)])


def Mouth():
    rect(screen, BLACK, (140, 220, 120, 20), 40)


screen = pygame.display.set_mode((400, 400))
screen.fill(WHITE)

Face()
Eyes()
Eyebrows()
Mouth()

FPS = 30

pygame.display.set_caption("Smile")
pygame.display.update()
clock = pygame.time.Clock()
finished = False

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
