import pygame
from pygame.draw import *

pygame.init()

# Цвета (R, G, B)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREY = (128, 128, 128)
# Main coords
CENTRE = (200, 175)

screen = pygame.display.set_mode((1920, 1080))


def BackGround():
    screen.fill(BLACK)
    rect(screen, GREY, (0, 0, 1920, 400))


def House(x, y):
    rect(screen, (150, 75, 0), (100+x, 300+y, 300, 300))
    for i in range(4):
        rect(screen, (78, 87, 84), (120 + 70 * i+x, 330+y, 40, 100))
    rect(screen, YELLOW, (310+x, 510+y, 40, 40))
    rect(screen, (184, 183, 153), (310 - 70+x, 510+y, 40, 40))
    rect(screen, (184, 183, 153), (310 - 70 - 70+x, 510+y, 40, 40))
    rect(screen, WHITE, (70+x, 430+y, 370, 10))
    rect(screen, WHITE, (100+x, 390+y, 300, 10))
    for j in range(5):
        rect(screen, WHITE, (100 + 70 * j+x, 390+y, 30, 50))
    polygon(screen, BLACK, ([80+x, 300+y], [100+x, 260+y], [400+x, 260+y], [420+x, 300+y]))


def clouds():
    a = 240
    circle(screen, WHITE, (700, 90), 50)
    for i in range(3):
        ellipse(screen, (122, 118, 102), (10 + a * i, 50, 280, 100))
    ellipse(screen, (122, 118, 102), (10 + 2 * a, 50 + 150, 280, 100))


def Ghost(x, y):
    polygon(screen, WHITE, (
        [600+x, 600+y], [620+x, 640+y], [640+x, 620+y], [660+x, 640+y], [680+x, 620+y], [700+x, 640+y], [720+x, 600+y], [680+x, 540+y], [680+x, 520+y]))
    circle(screen, WHITE, (660+x, 540+y), 40)
    circle(screen, RED, (670+x, 540+y), 5)
    circle(screen, RED, (650+x, 540+y), 5)


BackGround()
clouds()
for i in range(3):
    House(500 * i, 100 * i)
    Ghost(300 * i, 100 * i)
Ghost(-150,0)
FPS = 60

pygame.display.set_caption("Ghost")
pygame.display.update()
clock = pygame.time.Clock()
finished = False

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
