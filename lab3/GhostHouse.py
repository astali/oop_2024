import pygame
from pygame.draw import *

pygame.init()

# Цвета (R, G, B)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREY = (128, 128, 128)
# Main coords
CENTRE = (200, 175)

screen = pygame.display.set_mode((800, 800))


def BackGround():
    screen.fill(BLACK)
    rect(screen, GREY, (0, 0, 800, 400))


def House():
    rect(screen, (150, 75, 0), (100, 300, 300, 300))
    for i in range(4):
        rect(screen, (78, 87, 84), (120 + 70 * i, 330, 40, 100))
    rect(screen, YELLOW, (310, 510, 40, 40))
    rect(screen, (184, 183, 153), (310 - 70, 510, 40, 40))
    rect(screen, (184, 183, 153), (310 - 70 - 70, 510, 40, 40))
    rect(screen, WHITE, (70, 430, 370, 10))
    rect(screen, WHITE, ((100, 390, 300, 10)))
    for i in range(5):
        rect(screen, WHITE, (100 + 70 * i, 390, 30, 50))
    polygon(screen, BLACK, ([80, 300], [100, 260], [400, 260], [420, 300]))


def clouds():
    a = 240
    circle(screen,WHITE,(700,90),50)
    for i in range(3):
        ellipse(screen, (122, 118, 102), (10+a*i, 50, 280, 100))
    ellipse(screen, (122, 118, 102), (10 + 2*a, 50+150, 280, 100))

def Ghost():
    polygon(screen, WHITE,([600,600],[620,640],[640,620],[660,640],[680,620],[700,640],[720,600],[680,540],[680,520]))
    circle(screen,WHITE,(660,540),40)
    circle(screen,RED,(670,540),5)
    circle(screen,RED,(650,540),5)
BackGround()
House()
clouds()
Ghost()
FPS = 30

pygame.display.set_caption("Ghost")
pygame.display.update()
clock = pygame.time.Clock()
finished = False

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
